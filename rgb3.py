# script controls an rgb light
#!/usr/bin/env python
import RPi.GPIO as GPIO
import time
import random


def convert(input):
    return float((int(input)/255)*100)


def stop():
    pinR.stop()
    pinG.stop()
    pinB.stop()

    for i in pins:
        GPIO.setup(pins[i], GPIO.OUT)
        GPIO.setup(pins[i], GPIO.HIGH)

    GPIO.cleanup() 


pins = {'R':11, 'G':12, 'B':13}

GPIO.cleanup()
GPIO.setmode(GPIO.BOARD)

for i in pins:
    # print(pins[i])
    GPIO.setup(pins[i], GPIO.OUT)
    GPIO.output(pins[i], GPIO.HIGH)
    
pinR = GPIO.PWM(pins['R'], 2000)
pinG = GPIO.PWM(pins['G'], 2000)
pinB = GPIO.PWM(pins['B'], 5000)

pinR.start(0)
pinG.start(0)
pinB.start(0)

red = 0
green = 0
blue = 0

running = True;

try:
    
    while running:
        
        while red < 255:
            red = red + 1
            pinR.ChangeDutyCycle(convert(red))
            time.sleep(.01)
        
        while green < 255:
            green = green + 1
            pinG.ChangeDutyCycle(convert(green))
            time.sleep(.01)
            
        while red > 0:
            red = red - 1
            pinR.ChangeDutyCycle(convert(red))
            time.sleep(.01)    
            
        while blue < 255:
            blue = blue + 1
            pinB.ChangeDutyCycle(convert(blue))
            time.sleep(.01)
         
        while green > 0:
            green = green - 1
            pinG.ChangeDutyCycle(convert(green))
            time.sleep(.01) 
         
        while red < 255:
            red = red + 1
            pinR.ChangeDutyCycle(convert(red))
            time.sleep(.01)
            
        while blue > 0:
            blue = blue - 1
            pinB.ChangeDutyCycle(convert(blue))
            time.sleep(.01)   
        #running = False

    stop()

except KeyboardInterrupt:
    stop()
    