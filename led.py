#this version of the LED script connects the negative to ground
#!/usr/bin/env python
import RPi.GPIO as GPIO
import time


LedPin = 11 # pin 11

GPIO.setmode(GPIO.BOARD) # Numbers GPIOs by physical location
GPIO.setup(LedPin, GPIO.OUT)
GPIO.output(LedPin, GPIO.LOW)

try:
    while True:
        GPIO.output(LedPin, GPIO.HIGH) # led on
        print("led on")
        time.sleep(1)
        GPIO.output(LedPin, GPIO.LOW) # led off
        print("led off")
        time.sleep(1)
except KeyboardInterrupt:
    GPIO.output(LedPin, GPIO.LOW) # led off
    print("cleaning up")
    GPIO.cleanup() # release resources
    