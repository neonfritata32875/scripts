#Key presses
import RPi.GPIO as GPIO
import time


GPIO.cleanup()
KEY_RED = 12
KEY_BLUE = 16
LED_RED = 11
LED_BLUE = 13
GPIO.setmode(GPIO.BOARD)

GPIO.setup(KEY_RED, GPIO.IN, GPIO.PUD_UP)
GPIO.setup(LED_RED, GPIO.OUT)
GPIO.output(LED_RED, GPIO.HIGH)

GPIO.setup(KEY_BLUE, GPIO.IN, GPIO.PUD_UP)
GPIO.setup(LED_BLUE, GPIO.OUT)
GPIO.output(LED_BLUE, GPIO.HIGH)

red_off = True
blue_off = True

try:
    while True:
        time.sleep(0.05)
        if GPIO.input(KEY_RED) == GPIO.LOW and red_off:
            #print("KEY_RED PRESS")
            GPIO.output(LED_RED, GPIO.LOW)
            red_off = False
            #while GPIO.input(KEY_RED) == GPIO.LOW:
                #print("HOLDING")
                #time.sleep(0.1)
        elif GPIO.input(KEY_RED) == GPIO.HIGH:
            GPIO.output(LED_RED, GPIO.HIGH)
            red_off = True
            #print("NO PRESS")
        
        if GPIO.input(KEY_BLUE) == GPIO.LOW and blue_off:
           #print("KEY_BLUE PRESS")
           GPIO.output(LED_BLUE, GPIO.LOW)
           blue_off = False
        elif GPIO.input(KEY_BLUE) == GPIO.HIGH:
            GPIO.output(LED_BLUE, GPIO.HIGH)
            blue_off = True
            
except KeyboardInterrupt:
    GPIO.cleanup()
    