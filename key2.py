#Utilizes an on and off switch
import RPi.GPIO as GPIO
import time


GPIO.cleanup()
KEY_ON = 12
KEY_OFF = 16
LED_RED = 11
LED_BLUE = 13
GPIO.setmode(GPIO.BOARD)

GPIO.setup(KEY_ON, GPIO.IN, GPIO.PUD_UP)
GPIO.setup(LED_RED, GPIO.OUT)
GPIO.output(LED_RED, GPIO.HIGH)

GPIO.setup(KEY_OFF, GPIO.IN, GPIO.PUD_UP)
GPIO.setup(LED_BLUE, GPIO.OUT)
GPIO.output(LED_BLUE, GPIO.HIGH)

red_off = True
blue_off = True

try:
    while True:
        time.sleep(0.05)
        if GPIO.input(KEY_ON) == GPIO.LOW:
            #print("KEY_ON PRESS")
            GPIO.output(LED_RED, GPIO.LOW)
            GPIO.output(LED_BLUE, GPIO.LOW)
            
        elif GPIO.input(KEY_ON) == GPIO.HIGH:
            time.sleep(0.01)
        
        if GPIO.input(KEY_OFF) == GPIO.LOW:
            #print("KEY_OFF PRESS")
            GPIO.output(LED_RED, GPIO.HIGH)
            GPIO.output(LED_BLUE, GPIO.HIGH)
            
        elif GPIO.input(KEY_OFF) == GPIO.HIGH:
            time.sleep(0.01)
            
except KeyboardInterrupt:
    GPIO.cleanup()
    