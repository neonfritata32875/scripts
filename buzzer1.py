import RPi.GPIO as GPIO
import time


BeepPin = 11    # pin11
KeyPin = 12


def setup():
    GPIO.cleanup()
    GPIO.setmode(GPIO.BOARD)        # Numbers GPIOs by physical location
    GPIO.setup(BeepPin, GPIO.OUT)   # Set BeepPin's mode is output
    GPIO.output(BeepPin, GPIO.HIGH) # Set BeepPin high(+3.3V) to off beep
    GPIO.setup(KeyPin, GPIO.IN, GPIO.PUD_UP)
    
    
def loop():
    while True:
        
        #time.sleep(0.05)
        if GPIO.input(KeyPin) == GPIO.LOW:
            GPIO.output(BeepPin, GPIO.LOW)
            time.sleep(0.0001)
            GPIO.output(BeepPin, GPIO.HIGH)
            time.sleep(0.0001)
        else:
           GPIO.output(BeepPin, GPIO.HIGH) 
        
        
def destroy():
        GPIO.output(BeepPin, GPIO.HIGH)    # beep off
        GPIO.cleanup()                     # Release resource   if __name__ == '__main__':
        
# Program start from here
print('Press Ctrl+C to end the program...')
setup()

try:
    loop()
except KeyboardInterrupt:
    # When 'Ctrl+C' is pressed, the child program destroy() will be  executed.
    destroy() 
 