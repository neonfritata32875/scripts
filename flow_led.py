#!/usr/bin/env python
import RPi.GPIO as GPIO
import time


pins = [7, 11, 12, 13, 15, 16, 18, 22]

GPIO.setmode(GPIO.BOARD) # Numbers GPIOs by physical location

try:
    while True:
        for pin in pins:
            GPIO.setup(pin, GPIO.OUT)
            GPIO.output(pin, GPIO.LOW)
            time.sleep(.1)
            GPIO.output(pin,GPIO.HIGH)
except KeyboardInterrupt:
    for pin in pins:
            GPIO.output(pin,GPIO.HIGH)
    print("cleaning up")
    GPIO.cleanup() # release resources
    
# time.sleep(2)
