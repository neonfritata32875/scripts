# script controls an rgb light
#!/usr/bin/env python
import RPi.GPIO as GPIO
import time
import random


def convert(input):
    return float((int(input)/255)*100)


pins = {'R':11, 'G':12, 'B':13}

GPIO.cleanup()
GPIO.setmode(GPIO.BOARD)

for i in pins:
    # print(pins[i])
    GPIO.setup(pins[i], GPIO.OUT)
    GPIO.output(pins[i], GPIO.HIGH)
    
pinR = GPIO.PWM(pins['R'], 2000)
pinG = GPIO.PWM(pins['G'], 2000)
pinB = GPIO.PWM(pins['B'], 5000)

pinR.start(0)
pinG.start(0)
pinB.start(0)

red = 0
green = 0
blue = 0

try:
    
    while True:
        
        
        red = convert(random.randrange(0,255))
        green = convert(random.randrange(0,255))
        blue = convert(random.randrange(0,255))
        
        print(red,green,blue)
        
        pinR.ChangeDutyCycle(red)
        pinG.ChangeDutyCycle(green)
        pinB.ChangeDutyCycle(blue)
    
        time.sleep(.5)
    
except KeyboardInterrupt:
    pinR.stop()
    pinG.stop()
    pinB.stop()
    
    for i in pins:
        GPIO.setup(pins[i], GPIO.OUT)
        GPIO.setup(pins[i], GPIO.HIGH)
    
    GPIO.cleanup()
    