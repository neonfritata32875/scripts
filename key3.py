#RGB Switches!
import RPi.GPIO as GPIO
import time


def setup_keys():
    GPIO.setup(keys['R'], GPIO.IN, GPIO.PUD_UP)
    GPIO.setup(keys['G'], GPIO.IN, GPIO.PUD_UP)
    GPIO.setup(keys['B'], GPIO.IN, GPIO.PUD_UP)


def setup_pins():
    for i in pins:
        #print(pins[i])
        GPIO.setup(pins[i], GPIO.OUT)
        GPIO.output(pins[i], GPIO.HIGH)


GPIO.cleanup()

keys = {'R':12, 'G':16, 'B':18}
pins = {'R':15, 'G':13, 'B':11}

GPIO.setmode(GPIO.BOARD)

setup_keys()

setup_pins()

pinR = GPIO.PWM(pins['R'], 2000)
pinG = GPIO.PWM(pins['G'], 2000)
pinB = GPIO.PWM(pins['B'], 5000)

pinR.start(0)
pinG.start(0)
pinB.start(0)

try:
    while True:
        time.sleep(0.05)
        if GPIO.input(keys['R']) == GPIO.LOW:
            print("keys['R'] PRESS")
            pinR.ChangeDutyCycle(100)
            
        elif GPIO.input(keys['R']) == GPIO.HIGH:
            pinR.ChangeDutyCycle(0)
            #time.sleep(0.05)
        
        if GPIO.input(keys['G']) == GPIO.LOW:
            print("keys['G'] PRESS")
            pinG.ChangeDutyCycle(100)
            
        elif GPIO.input(keys['G']) == GPIO.HIGH:
            pinG.ChangeDutyCycle(0)
            #time.sleep(0.05)
            
        if GPIO.input(keys['B']) == GPIO.LOW:
            print("keys['B'] PRESS")
            pinB.ChangeDutyCycle(100)
            
        elif GPIO.input(keys['B']) == GPIO.HIGH:
            pinB.ChangeDutyCycle(0)
            #time.sleep(0.05)
            
except KeyboardInterrupt:
    GPIO.cleanup()
    